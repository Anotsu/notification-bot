﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NotificationBot.NotificationSource.Core.Email;

namespace NotificationBot.NotificationSource.Yahoo
{
    /// <summary>
    /// Notificaiton source to recive notifications from yahoo email
    /// </summary>
    public class YahooNotificationSource : EmailNotificationSource<YahooNotificationConfiguration>
    {
        /// <inheritdoc cref="YahooNotificationSource"/>
        public YahooNotificationSource(
            IOptions<YahooNotificationConfiguration> configuration,
            ILogger<EmailNotificationSubscription> subscriptionLogger)
            : base(configuration, subscriptionLogger)
        {
        }
    }
}