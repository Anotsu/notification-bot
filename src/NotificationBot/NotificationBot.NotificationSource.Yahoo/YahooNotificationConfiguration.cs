﻿using NotificationBot.NotificationSource.Core.Email;

namespace NotificationBot.NotificationSource.Yahoo
{
    /// <summary>
    /// Configuration for email notifications
    /// </summary>
    public class YahooNotificationConfiguration : EmailNotificationConfiguration
    {
        
    }
}