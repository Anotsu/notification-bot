﻿using System;
using Microsoft.Extensions.Configuration;

namespace NotificationBot.Common.Exceptions
{
    /// <summary>
    /// Thrown when configuration does not exist
    /// </summary>
    public class ConfigurationNotFoundException : Exception
    {
        /// <inheritdoc cref="ConfigurationNotFoundException"/>
        public ConfigurationNotFoundException(IConfigurationSection configurationSection)
            : base($"Configuration ${configurationSection.Path} not found")
        {
        }
    }
}