﻿using Microsoft.Extensions.Configuration;
using NotificationBot.Common.Exceptions;

namespace NotificationBot.Common.Extensions
{
    /// <summary>
    /// Extension methods for <see cref="IConfiguration"/> and <see cref="IConfigurationSection"/>
    /// </summary>
    public static class ConfigurationExtensions
    {
        /// <summary>
        /// Checks if configuration exists
        /// </summary>
        public static IConfigurationSection CheckExistence(this IConfigurationSection configurationSection)
        {
            return configurationSection.Exists()
                ? configurationSection
                : throw new ConfigurationNotFoundException(configurationSection);
        }
    }
}