﻿using System;

namespace NotificationBot.NotificationSource.Core
{
    /// <summary>
    /// Subscription to certain type of notifications
    /// </summary>
    public interface INotificationSubscription<TNotification> : IAsyncDisposable
        where TNotification : INotification
    {
        /// <summary>
        /// Returns reader for this subscription
        /// </summary>
        INotificationSubscriptionReader<TNotification> Reader { get; }

        /// <summary>
        /// Returns writer for this subscription
        /// </summary>
        INotificationSubscriptionWriter<TNotification> Writer { get; }
    }
}