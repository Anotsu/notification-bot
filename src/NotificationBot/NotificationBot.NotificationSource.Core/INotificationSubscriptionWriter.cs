﻿using System.Threading.Tasks;

namespace NotificationBot.NotificationSource.Core
{
    /// <summary>
    /// Writer for subscription
    /// </summary>
    public interface INotificationSubscriptionWriter<TNotification>
        where TNotification : INotification
    {
        /// <summary>
        /// Writes notificaion to subscription
        /// </summary>
        ValueTask WriteAsync(TNotification notification);
    }
}