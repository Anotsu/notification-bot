﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using NotificationBot.Common.Extensions;

namespace NotificationBot.NotificationSource.Core
{
    /// <summary>
    /// Extensions for core notification source functionality
    /// </summary>
    public static class Extensions
    {
        public static IServiceCollection AddNotificationSource<TNotificationSource, TConfiguration, TNotification>(
            this IServiceCollection services,
            IConfigurationSection configurationSection)
            where TNotificationSource : INotificationSource<TConfiguration, TNotification>
            where TConfiguration : NotificationConfiguration
            where TNotification : INotification
        {
            services.Configure<TConfiguration>(configurationSection.CheckExistence());
            services.TryAddScoped(typeof(INotificationSource<TConfiguration, TNotification>), typeof(TNotificationSource));

            return services;
        }
    }
}