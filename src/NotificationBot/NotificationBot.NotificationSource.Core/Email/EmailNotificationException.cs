﻿using System;

namespace NotificationBot.NotificationSource.Core.Email
{
    /// <summary>
    /// Exception thrown when error occured during interaction with <see cref="IEmailNotificationSource{TConfiguration}"/>
    /// </summary>
    public class EmailNotificationException : Exception
    {
        /// <inheritdoc cref="EmailNotificationException"/>
        public EmailNotificationException(string host, Exception ex)
            : base($"An error occured during interaction with IMAP server: {host}", ex)
        {
        }
    }
}