﻿using System;
using System.Linq;
using System.Threading.Tasks;
using MailKit.Net.Imap;
using Microsoft.Extensions.Logging;
using MimeKit;

namespace NotificationBot.NotificationSource.Core.Email
{
    /// <summary>
    /// Subscription for email notifications
    /// </summary>
    public class EmailNotificationSubscription : NotificationSubscription<EmailNotification>
    {
        private readonly ImapClient _imapClient;
        private readonly ILogger<EmailNotificationSubscription> _logger;

        /// <inheritdoc cref="EmailNotificationSubscription"/>
        public EmailNotificationSubscription(ImapClient imapClient, ILogger<EmailNotificationSubscription> logger)
        {
            _imapClient = imapClient;
            _logger = logger;
            Writer = new DefaultNotificationWriter<EmailNotification>(this);
            Reader = new DefaultNotificationReader<EmailNotification>(this);
            _imapClient.Inbox.UnreadChanged += InboxOnUnreadChanged;
        }

        /// <inheritdoc />
        public override ValueTask DisposeAsync()
        {
            _imapClient.Dispose();
            Items.Writer.Complete();
            _imapClient.Inbox.UnreadChanged -= InboxOnUnreadChanged;
            return ValueTask.CompletedTask;
        }

        /// <summary>
        /// Triggers when unread mails count changed
        /// </summary>
        private void InboxOnUnreadChanged(object? sender, EventArgs e)
        {
            // sync version of GetMessage due to synchronous event...
            // gotta find another way to call GetMessageAsync
            var message = _imapClient.Inbox.GetMessage(_imapClient.Inbox.Count - 1);

            var from = (MailboxAddress) message.From.First();

            var notification = new EmailNotification
            {
                From = new EmailAddress
                {
                    Address = from.Address,
                    Name = from.Name
                },
                Subject = message.Subject
            };

            if (!Items.Writer.TryWrite(notification))
                _logger.LogError("Could not write email notification to the channel: {emailNotification}", notification);
        }
    }
}