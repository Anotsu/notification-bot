﻿namespace NotificationBot.NotificationSource.Core.Email
{
    /// <summary>
    /// Represents internet address
    /// </summary>
    public class EmailAddress
    {
        /// <summary>
        /// Email
        /// </summary>
        public string Name { get; init; }

        /// <summary>
        /// Email address
        /// </summary>
        public string Address { get; init; }
    }
}