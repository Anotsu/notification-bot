﻿namespace NotificationBot.NotificationSource.Core.Email
{
    /// <summary>
    /// Notification source for emails
    /// </summary>
    public interface IEmailNotificationSource<TConfiguration> : INotificationSource<TConfiguration, EmailNotification>
        where TConfiguration : EmailNotificationConfiguration
    {
    }
}