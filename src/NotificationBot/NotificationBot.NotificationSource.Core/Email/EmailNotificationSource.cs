﻿using System;
using System.Net.Sockets;
using System.Threading.Tasks;
using MailKit.Net.Imap;
using MailKit.Security;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace NotificationBot.NotificationSource.Core.Email
{
    /// <inheritdoc cref="IEmailNotificationSource{TConfiguration}"/>
    public abstract class EmailNotificationSource<TConfiguration> : IEmailNotificationSource<TConfiguration>
        where TConfiguration : EmailNotificationConfiguration
    {
        protected readonly IOptions<TConfiguration> Configuration;
        protected readonly ILogger<EmailNotificationSubscription> SubscriptionLogger;

        /// <inheritdoc cref="EmailNotificationSource{TConfiguration}"/>
        protected EmailNotificationSource(IOptions<TConfiguration> configuration, ILogger<EmailNotificationSubscription> subscriptionLogger)
        {
            Configuration = configuration;
            SubscriptionLogger = subscriptionLogger;
        }

        /// <inheritdoc />
        public async Task<INotificationSubscription<EmailNotification>> SubscribeAsync()
        {
            var imapClient = new ImapClient();

            try
            {
                await imapClient.ConnectAsync(Configuration.Value.Host, Configuration.Value.Port);
                await imapClient.AuthenticateAsync(Configuration.Value.EmailAddress, Configuration.Value.Password);

                var subscription = new EmailNotificationSubscription(imapClient, SubscriptionLogger);

                return subscription;
            }
            catch (Exception ex) when (ex is AuthenticationException or SocketException)
            {
                throw new EmailNotificationException(Configuration.Value.Host, ex);
            }
        }
    }
}