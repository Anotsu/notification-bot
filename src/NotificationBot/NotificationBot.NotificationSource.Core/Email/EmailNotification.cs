﻿namespace NotificationBot.NotificationSource.Core.Email
{
    /// <summary>
    /// Notification from email
    /// </summary>
    public class EmailNotification : INotification
    {
        /// <summary>
        /// Subject of email
        /// </summary>
        public string Subject { get; set; }
        
        /// <summary>
        /// Email address of who sent mail
        /// </summary>
        public EmailAddress From { get; set; }
    }
}