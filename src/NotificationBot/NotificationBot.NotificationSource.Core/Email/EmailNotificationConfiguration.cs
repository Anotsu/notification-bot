﻿namespace NotificationBot.NotificationSource.Core.Email
{
    /// <summary>
    /// Configuration for email notifications
    /// </summary>
    public abstract class EmailNotificationConfiguration : NotificationConfiguration
    {
        /// <summary>
        /// IMAP server
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// Port of IMAP server
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Email address to recieive notifications from
        /// </summary>
        public string EmailAddress { get; set; }

        /// <summary>
        /// Email address password
        /// </summary>
        public string Password { get; set; }
    }
}