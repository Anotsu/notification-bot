﻿using System.Threading;
using System.Threading.Tasks;

namespace NotificationBot.NotificationSource.Core
{
    /// <summary>
    /// Reader for subscription
    /// </summary>
    public interface INotificationSubscriptionReader<TNotification>
        where TNotification : INotification
    {
        /// <summary>
        /// Reads notification from <see cref="INotificationSubscription{TNoficication}"/>
        /// </summary>
        /// <param name="cancellationToken"> Cancellation token to stop reading notifications </param>
        ValueTask<TNotification> ReadAsync(CancellationToken cancellationToken);
    }
}