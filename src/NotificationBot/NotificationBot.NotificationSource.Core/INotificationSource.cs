﻿using System.Threading.Tasks;

namespace NotificationBot.NotificationSource.Core
{
    /// <summary>
    /// Source of notifications
    /// </summary>
    public interface INotificationSource<TConfiguration, TNotification>
        where TConfiguration : NotificationConfiguration
        where TNotification : INotification
    {
        /// <summary>
        /// Subscribes to source and returns <see cref="INotificationSubscription{TNotifiction}"/> to read notifications from
        /// </summary>
        Task<INotificationSubscription<TNotification>> SubscribeAsync();
    }
}