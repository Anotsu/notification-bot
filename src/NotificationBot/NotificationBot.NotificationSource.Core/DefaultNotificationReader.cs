﻿using System.Threading;
using System.Threading.Tasks;

namespace NotificationBot.NotificationSource.Core
{
    /// <summary>
    /// Default notification reader
    /// </summary>
    public class DefaultNotificationReader<TNotification> : INotificationSubscriptionReader<TNotification>
        where TNotification : INotification
    {
        private readonly NotificationSubscription<TNotification> _subscription;

        /// <summary>
        /// <inheritdoc cref="DefaultNotificationReader{TNotification}"/>
        /// </summary>
        public DefaultNotificationReader(NotificationSubscription<TNotification> subscription)
        {
            _subscription = subscription;
        }

        /// <inheritdoc />
        public async ValueTask<TNotification> ReadAsync(CancellationToken cancellationToken)
        {
            return await _subscription.Items.Reader.ReadAsync(cancellationToken);
        }
    }
}