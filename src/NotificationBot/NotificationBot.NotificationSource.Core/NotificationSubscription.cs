﻿using System.Threading.Channels;
using System.Threading.Tasks;

namespace NotificationBot.NotificationSource.Core
{
    /// <summary>
    /// Base class for notification subscription
    /// </summary>
    public abstract class NotificationSubscription<TNotification> : INotificationSubscription<TNotification>
        where TNotification : INotification
    {
        /// <summary>
        /// <inheritdoc cref="NotificationSubscription{TNotification}"/>
        /// </summary>
        protected NotificationSubscription()
        {
            Items = Channel.CreateUnbounded<TNotification>();
        }

        /// <summary>
        /// Channel for notifications
        /// </summary>
        public Channel<TNotification> Items { get; }

        /// <inheritdoc />
        public INotificationSubscriptionReader<TNotification> Reader { get; protected init; }

        /// <inheritdoc />
        public INotificationSubscriptionWriter<TNotification> Writer { get; protected init; }

        /// <inheritdoc />
        public abstract ValueTask DisposeAsync();
    }
}