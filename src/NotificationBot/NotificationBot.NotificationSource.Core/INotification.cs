﻿namespace NotificationBot.NotificationSource.Core
{
    /// <summary>
    /// Notification from <see cref="INotificationSource{TOptions, TNofication}"/>
    /// </summary>
    public interface INotification
    {
    }
}