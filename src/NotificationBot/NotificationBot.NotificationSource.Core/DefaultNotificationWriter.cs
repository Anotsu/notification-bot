﻿using System.Threading.Tasks;

namespace NotificationBot.NotificationSource.Core
{
    /// <summary>
    /// Default writer for notifications
    /// </summary>
    /// <typeparam name="TNotification"></typeparam>
    public class DefaultNotificationWriter<TNotification> : INotificationSubscriptionWriter<TNotification>
        where TNotification : INotification

    {
        private readonly NotificationSubscription<TNotification> _subscription;

        /// <inheritdoc cref="DefaultNotificationWriter{TNotification}"/>
        public DefaultNotificationWriter(NotificationSubscription<TNotification> subscription)
        {
            _subscription = subscription;
        }

        /// <inheritdoc />
        public ValueTask WriteAsync(TNotification notification)
        {
            return _subscription.Items.Writer.WriteAsync(notification);
        }
    }
}