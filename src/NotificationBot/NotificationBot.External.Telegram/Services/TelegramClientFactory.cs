﻿using System.Collections.Concurrent;
using System.Net.Http;
using Telegram.Bot;

namespace NotificationBot.External.Telegram.Services
{
    /// <inheritdoc cref="ITelegramClientFactory"/>
    public class TelegramClientFactory : ITelegramClientFactory
    {
        private readonly ConcurrentDictionary<string, TelegramBotClient> _clientsCache = new();

        private readonly IHttpClientFactory _httpClientFactory;

        /// <inheritdoc cref="TelegramClientFactory"/>
        public TelegramClientFactory(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        /// <inheritdoc />
        public TelegramBotClient CreateClient(string apiToken)
        {
            var httpClient = _httpClientFactory.CreateClient();

            return _clientsCache.GetOrAdd(apiToken, key => new TelegramBotClient(apiToken, httpClient));
        }
    }
}