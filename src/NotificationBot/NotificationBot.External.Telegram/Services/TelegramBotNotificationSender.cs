﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NotificationBot.External.Telegram.Exceptions;
using Telegram.Bot.Exceptions;

namespace NotificationBot.External.Telegram.Services
{
    /// <inheritdoc cref="ITelegramBotNotificationSender"/>
    public class TelegramBotNotificationSender : ITelegramBotNotificationSender
    {
        private readonly ITelegramClientFactory _telegramClientFactory;
        private readonly ILogger<TelegramBotNotificationSender> _logger;

        /// <inheritdoc cref="TelegramBotNotificationSender"/>
        public TelegramBotNotificationSender(ITelegramClientFactory telegramClientFactory, ILogger<TelegramBotNotificationSender> logger)
        {
            _telegramClientFactory = telegramClientFactory;
            _logger = logger;
        }

        /// <inheritdoc />
        public async Task SendAsync(string apiToken, string pushId, string subject, string notification)
        {
            var telegramBotClient = _telegramClientFactory.CreateClient(apiToken);

            try
            {
                await telegramBotClient.SendTextMessageAsync(pushId, notification);
            }
            catch (ApiRequestException ex)
            {
                var exception = new TelegramBotExternalInteropException(pushId, "An error occured during sending message to telegram bot", ex);
                _logger.LogError(exception, "An error occured during sending message to telegram bot");
                throw exception;
            }
        }
    }
}