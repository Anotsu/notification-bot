﻿using NotificationBot.External.Core;

namespace NotificationBot.External.Telegram.Services
{
    /// <summary>
    /// Service for sending notifications to telegram bot
    /// </summary>
    public interface ITelegramBotNotificationSender : INotificationSender
    {
    }
}