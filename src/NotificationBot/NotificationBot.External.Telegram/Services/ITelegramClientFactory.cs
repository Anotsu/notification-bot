﻿using Telegram.Bot;

namespace NotificationBot.External.Telegram.Services
{
    /// <summary>
    /// Factory for <see cref="TelegramBotClient"/>
    /// </summary>
    public interface ITelegramClientFactory
    {
        /// <summary>
        /// Creates instance of <see cref="TelegramBotClient"/> for given <paramref name="apiToken"/>
        /// </summary>
        TelegramBotClient CreateClient(string apiToken);
    }
}