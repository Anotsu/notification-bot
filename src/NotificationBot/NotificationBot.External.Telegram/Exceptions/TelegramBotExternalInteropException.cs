﻿using System;
using NotificationBot.External.Core.Exceptions;

namespace NotificationBot.External.Telegram.Exceptions
{
    /// <summary>
    /// Thrown when interop with telegram bot api failed
    /// </summary>
    public class TelegramBotExternalInteropException : ExternalInteropException
    {
        /// <inheritdoc cref="TelegramBotExternalInteropException"/>
        public TelegramBotExternalInteropException(string chatId, string message) : base(message)
        {
            ChatId = chatId;
        }

        /// <inheritdoc cref="TelegramBotExternalInteropException"/>
        public TelegramBotExternalInteropException(string chatId, string message, Exception innerException) : base(message, innerException)
        {
            ChatId = chatId;
        }

        /// <summary>
        /// ChatId where messaging tried to be sent
        /// </summary>
        public string ChatId { get; }
    }
}