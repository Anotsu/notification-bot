﻿using Microsoft.Extensions.DependencyInjection;
using NotificationBot.External.Core;
using NotificationBot.External.Telegram.Services;

namespace NotificationBot.External.Telegram
{
    /// <summary>
    /// Extension methods for external interop via telegram
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Adds <see cref="ITelegramClientFactory"/> to <see cref="IServiceCollection"/>
        /// </summary>
        public static IServiceCollection AddTelegramBot(this IServiceCollection services)
        {
            services.AddHttpClient();

            return services.AddSingleton<ITelegramClientFactory, TelegramClientFactory>()
                .AddScoped<INotificationSender, TelegramBotNotificationSender>()
                .AddSingleton<ITelegramBotNotificationSender, TelegramBotNotificationSender>();
        }
    }
}