﻿using System.Threading.Tasks;

namespace NotificationBot.External.Core
{
    /// <summary>
    /// Service for sending notifications
    /// </summary>
    public interface INotificationSender
    {
        /// <summary>
        /// Sends notification to certain api
        /// </summary>
        /// <param name="apiToken"> Api token to send notification to </param>
        /// <param name="pushId"> Id used to identify where to push exactly. for example certain telegram chat, channel, discord group etc... </param>
        /// <param name="subject"> Subject of notification </param>
        /// <param name="notification"> Actual notification message </param>
        Task SendAsync(string apiToken, string pushId, string subject, string notification);
    }
}