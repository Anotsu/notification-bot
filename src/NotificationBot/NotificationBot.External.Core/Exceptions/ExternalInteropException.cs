﻿using System;

namespace NotificationBot.External.Core.Exceptions
{
    /// <summary>
    /// Thrown when interop with external source failed
    /// </summary>
    public class ExternalInteropException : Exception
    {
        /// <inheritdoc cref="ExternalInteropException"/>
        public ExternalInteropException(string message) : base(message)
        {
        }

        /// <inheritdoc cref="ExternalInteropException"/>
        public ExternalInteropException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}